<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/post/{id}', 'PostController@show');

Route::get('/listar-publicaciones', 'PostController@listaPublicados');

Route::get('/post/comentarios/listar/{post_id}', 'PostCommentController@comments');

Route::group(['middleware' => ['auth']], function() {

    Route::get('/publicar', 'HomeController@index')->name('home');

    Route::get('/posts', 'PostController@list');
    
    
    Route::post('/post/editar', 'PostController@update');
    
    Route::post('/post/guardar', 'PostController@save');
    
    Route::post('/post/aprobar', 'PostController@AprobarPost');
    
    Route::post('/post/rechazar', 'PostController@RechazarPost');
    
    Route::post('/post/eliminar', 'PostController@destroy');

    Route::post('/post/comentario/guardar', 'PostCommentController@save');
    Route::post('/post/me-gusta/guardar', 'PostLikeController@save');
    
    Route::get('/vista-previa/{id}', 'PostController@preview');

    Route::get('/mi-perfil', 'UserController@perfil');
    Route::post('/mi-perfil/editar', 'UserController@editar');

});