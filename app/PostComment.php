<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    public function post(){
    	return $this->belongsTo('App\Post');
    }

    public function user(){
    	return $this->belongsto('App\User');
    }
}
