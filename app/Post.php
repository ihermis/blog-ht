<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user(){
    	return $this->belongsto('App\User');
    }

    public function comentarios(){
    	return $this->hasMany('App\PostComment');
    }

    public function likes(){
    	return $this->hasMany('App\PostLike');
    }
}
