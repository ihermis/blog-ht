<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Image;

class UserController extends Controller
{
    /**
     * Cargar datos del usuario activo
     *
     * @return void
     */
    public function perfil()
    {
        $data['user'] = User::where('id', auth()->user()->id)->get();

        return view('perfil', $data);
    }

    /**
     * Editar datos básicos del usuario
     *
     * @param  Request  $request
     * @return array
     */
    public function editar(Request $request)
    {
        $user = User::findOrFail($request->id);
        $foto = '';
        $subirFoto = false;
        $img_eliminar = '';
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $foto = $file->getClientOriginalName();
            $img_eliminar = $user->pic;
            $subirFoto = true;
        }


        $user->name = $request->input('nombre');
        $user->surname = $request->input('apellido');
        $user->pic = $foto;

        if ($user->save()) {
            if ($subirFoto) {
                $ruta = public_path('/img/usuario/' . $user->id . '/' . $foto);
                if (!file_exists(public_path('/img/usuario/' . $user->id))) {
                    @mkdir(public_path('/img/usuario/' . $user->id), 777, true);
                }
                Image::make($file->getRealPath())->save($ruta, 50);

                if (!is_null($img_eliminar) && $img_eliminar != '' && $img_eliminar != "sin_img.png") {
                    $file_rm = public_path('/img/usuario/' . $user->id . '/' . $img_eliminar);
                    if (file_exists($file_rm)) {
                        unlink($file_rm);
                    }
                }
            }

            $data = [
                "status" => true,
            ];
        } else
            $data = [
                "status" => false,
            ];
        return $data;
    }
}
