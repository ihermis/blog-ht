<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostLike;

class PostLikeController extends Controller
{
    /**
     * Guardar un like dado a un post, y retorna un array con todos los likes de un post especifíco 
     *
     * @param  Request  $request
     * @return array
     */
    public function save(Request $request)
    {
        $like = new PostLike();
        $like->user_id = $request->input('user_id');
        $like->post_id = $request->input('post_id');
       
        if($like->save()){
            $data = [
                "status" => true,
                "array" => $like::where('post_id' , $request->input('post_id') )->get()
            ];
        }else
            $data = [
                "status" => false,
            ];
        return $data;
    }
}
