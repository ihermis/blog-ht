<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostComment;

class PostCommentController extends Controller
{

    /**
     * Se guardar un nuevo comentario de un post en la BD
     *
     * @param  Request  $request
     * @return array
     */
    public function save(Request $request)
    {
        $comment = new PostComment();
        $comment->user_id = $request->input('user_id');
        $comment->post_id = $request->input('post_id');
        $comment->comment = $request->input('comentario');

        if ($comment->save()) {
            $data = [
                "status" => true,
                "array" => $comment::where('post_id', $request->input('post_id'))->with('user')->get()
            ];
        } else
            $data = [
                "status" => false,
            ];
        return $data;
    }

    /**
     * Consulta los comentarios de un post especifíco 
     *
     * @param  Int  $post_id
     * @return array
     */
    public function comments($post_id)
    {
        $comment = PostComment::where('post_id', $post_id)->with('user')->get();
        return $comment;
    }
}
