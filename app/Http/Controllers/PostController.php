<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Image;

class PostController extends Controller
{

    /**
     * Consultar los post de un usuario / admin
     *
     * @param  Request  $request
     * @return array
     */
    public function list(Request $request)
    {
        $post = Post::with('user', 'comentarios', 'likes');
        if (auth()->user()->role != 'admin')
            $post = $post->where('user_id', auth()->user()->id);

        $post = $post->get();
        return $post;
    }

    /**
     * Consultar los post activos / publicados  para la vista principal del sitio
     *
     * @return array
     */
    public function listaPublicados()
    {
        $post = Post::where('status', 'publicar')->with('user', 'comentarios', 'likes')->get();
        return $post;
    }

    /**
     * Guardar nuevo post en la BD
     *
     * @param  Request  $request
     * @return array
     */
    public function save(Request $request)
    {
        $post = new Post();
        $img_principal = '';
        $subirFoto = false;
        if ($request->hasFile('imagen_principal')) {
            $file = $request->file('imagen_principal');
            $img_principal = $file->getClientOriginalName();
            $subirFoto = true;
        } else
            $img_principal = "sin_img.png";

        if ($request->input('estado_publicacion') == "publicar") {
            $estado_publicacion = 'pendiente';
        } else
        if ($request->input('estado_publicacion') == "publicar" && auth()->user()->role == 'admin') {
            $estado_publicacion = 'publicar';
        } else {
            $estado_publicacion = $request->input('estado_publicacion');
        }

        $post->title = $request->input('titulo');
        $post->body = $request->input('cuerpo');
        $post->status = $estado_publicacion;
        $post->main_image = $img_principal;
        $post->user_id = auth()->user()->id;
        $post->publication_date = date('Y-m-d');
        if ($post->save()) {
            if ($subirFoto) {
                $ruta = public_path('/img/post/' . $post->id . '/' . $img_principal);
                if (!file_exists(public_path('/img/post/' . $post->id))) {
                    @mkdir(public_path('/img/post/' . $post->id), 777, true);
                }
                Image::make($file->getRealPath())->save($ruta, 50);
            }

            $data = [
                "status" => true,
            ];
        } else
            $data = [
                "status" => false,
            ];
        return $data;
    }

    /**
     * Editar un post en la BD
     *
     * @param  Request  $request
     * @return array
     */
    public function update(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $img_principal = '';
        $subirFoto = false;
        $img_eliminar = '';
        if ($request->hasFile('imagen_principal')) {
            $file = $request->file('imagen_principal');
            $img_principal = $file->getClientOriginalName();
            $img_eliminar = $post->main_image;
            $subirFoto = true;
        } else
            $img_principal = "sin_img.png";

        if ($request->input('estado_publicacion') == "publicar" && $post->status != "publicar") {
            $estado_publicacion = 'pendiente';
        } else {
            $estado_publicacion = $request->input('estado_publicacion');
        }

        $post->title = $request->input('titulo');
        $post->body = $request->input('cuerpo');
        $post->status = $estado_publicacion;
        $post->main_image = $img_principal;

        if ($post->save()) {
            if ($subirFoto) {
                $ruta = public_path('/img/post/' . $post->id . '/' . $img_principal);
                if (!file_exists(public_path('/img/post/' . $post->id))) {
                    @mkdir(public_path('/img/post/' . $post->id), 777, true);
                }
                Image::make($file->getRealPath())->save($ruta, 50);

                if (!is_null($img_eliminar) && $img_eliminar != '' && $img_eliminar != "sin_img.png") {
                    $file_rm = public_path('/img/post/' . $post->id . '/' . $img_eliminar);
                    if (file_exists($file_rm)) {
                        unlink($file_rm);
                    }
                }
            }

            $data = [
                "status" => true,
            ];
        } else
            $data = [
                "status" => false,
            ];
        return $data;
    }

    /**
     * Aprobar un post pendiente por aprobar
     *
     * @param  Request  $request
     * @return array
     */
    public function AprobarPost(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $post->publication_date = date('Y-m-d');
        $post->status = "publicar";

        if ($post->save()) {
            $data = [
                "status" => true,
            ];
        } else
            $data = [
                "status" => false,
            ];
        return $data;
    }

    /**
     * Rechazar un post pendiente por aprobar /rechazar
     *
     * @param  Request  $request
     * @return array
     */
    public function RechazarPost(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $post->status = "rechazado";

        if ($post->save()) {
            $data = [
                "status" => true,
            ];
        } else
            $data = [
                "status" => false,
            ];
        return $data;
    }

    /**
     * Borrar un post de la BD
     *
     * @param  Request  $request
     * @return array
     */
    public function destroy(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $img_eliminar = $post->main_image;

        if ($post->delete()) {
            if (!is_null($img_eliminar) && $img_eliminar != '' && $img_eliminar != "sin_img.png") {
                $file_rm = public_path('/img/post/' . $request->id . '/' . $img_eliminar);
                if (file_exists($file_rm)) {
                    unlink($file_rm);
                }
            }
            $data = [
                "status" => true,
            ];
        } else
            $data = [
                "status" => false,
            ];
        return $data;
    }

    /**
     * Consultar y mostrar datos de un post ya publicado
     *
     * @param  Int  $id
     * @return array
     */
    public function show($id)
    {
        $post = Post::where('id', $id)->with('user', 'comentarios', 'likes')->get();
        $data['post'] = $post;
        $data['user'] = (isset(auth()->user()->id)) ? auth()->user()->id : 0;
        return view('read-post', $data);
    }

    /**
     * Consultar y mostrar datos de un post sin publicar
     *
     * @param  Int  $id
     * @return array
     */
    public function preview($id)
    {
        $post = Post::where('id', $id)->with('user', 'comentarios', 'likes')->get();
        $data['post'] = $post;
        $data['user'] = (isset(auth()->user()->id)) ? auth()->user()->id : 0;
        return view('vista_previa', $data);
    }
}
