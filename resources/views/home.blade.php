@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-12">
               <crud-component rol_ser="{{ Auth::user()->role }}" user_id="{{ Auth::user()->id }}"></crud-component>
            </div>
    </div>
</div>
@endsection
