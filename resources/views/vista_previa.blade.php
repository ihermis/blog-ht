@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-12">
               <post array_post="{{ $post }}" :user_id="{{ $user }}" :vista_previa="true"></post>
            </div>
    </div>
</div>
@endsection