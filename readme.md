## Blog HT

Simple app para simular un blog, gestionar publicaciones, comentarios, likes, etc.

## Herramientas usadas

-Laravel 5.8

-VueJS

-Bootstrap

## Atención!!

Ejecutar los siguientes comandos luego de clonar el proyecto.
```bash
composer update
```
```bash
npm install
```
```bash
npm run dev 
```

Editar el archivo .env con los datos de tu BD
```bash
php artisan migrate:fresh
```
```bash
php artisan db:seed

```

Para ejecutar en local


```bash
php artisan serve
```
## Capture del Modelo relacional

[https://gyazo.com/cde15e91aa9181a05a8c7d6d36f74398](https://gyazo.com/cde15e91aa9181a05a8c7d6d36f74398)

## Acceso admin de prueba

- useradmin@ejemplo.com

- pass: 123123

## Capture del Modelo relacional

[https://gyazo.com/40d315e82c1dd5071f4413623e1588cb](https://gyazo.com/40d315e82c1dd5071f4413623e1588cb)

[https://gyazo.com/3fc3cdf0d5ebbd05b6e788d534ebc4d5](https://gyazo.com/3fc3cdf0d5ebbd05b6e788d534ebc4d5)

[https://gyazo.com/de232ad8f50f4c2d26ef4c5f24660045](https://gyazo.com/de232ad8f50f4c2d26ef4c5f24660045)
