<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->id = 1;
        $user->name = 'Admin';
        $user->surname = '1';
        $user->email  = "useradmin@ejemplo.com";
        $user->password = bcrypt("123123");
        $user->role = 'admin';
        $user->save();
    }
}
